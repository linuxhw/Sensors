Coolest Computers
=================

WARNING: This report is not ready yet. The data is still processed. The report represents only last month of user probes.

This is a project to find coolest computers with minimal operation
CPU temperature reached and share [lm_sensors](https://github.com/lm-sensors/lm-sensors) reports collected by
Linux users at https://linux-hardware.org.

Everyone can contribute to this repository by uploading probes of their computers
by the [hw-probe](https://github.com/linuxhw/hw-probe) tool:

    sudo -E hw-probe -all -upload

Total reports: 207286.

Contents
--------

1. [ About        ](#about)
2. [ Notebooks    ](#notebooks)
3. [ Convertibles ](#convertibles)
4. [ Desktops     ](#desktops)
5. [ All In Ones  ](#all-in-ones)
6. [ Servers      ](#servers)
7. [ Mini Pcs     ](#mini-pcs)
8. [ Tablets      ](#tablets)

About
-----

The structure of the directory is the following:

    {TYPE}/{VENDOR}/{MODEL PREFIX}/{MODEL}/{HWID}

    ( e.g. Notebook/Lenovo/G570/G570 20079/87FB42D5B9E2 )

Notebooks
---------

| MFG                      | Model                                            | Min Temp | Avg Temp | Samples | HWID                                                                                                    |
|--------------------------|--------------------------------------------------|----------|----------|---------|---------------------------------------------------------------------------------------------------------|
| Samsung Electronics      | NC10                                             | 8        | 29.5     | 6       | [9269](<Notebook/Samsung Electronics/NC/NC10/926952B48F6F>)                                             |
| Dell                     | Latitude 2100                                    | 13       | 13       | 1       | [DED7](<Notebook/Dell/Latitude/Latitude 2100/DED7CFD6BA3E>)                                             |
| Acer                     | Aspire 6935                                      | 19       | 19       | 1       | [A649](<Notebook/Acer/Aspire/Aspire 6935/A649D1AD9AC3>)                                                 |
| Acer                     | Aspire 8920                                      | 20       | 29.5     | 2       | [11B4](<Notebook/Acer/Aspire/Aspire 8920/11B4C324E2E8>)                                                 |
| Lenovo                   | ThinkBook 16p Gen 2 20YM                         | 20       | 20       | 2       | [61EB](<Notebook/Lenovo/ThinkBook/ThinkBook 16p Gen 2 20YM/61EB408921F7>)                               |
| Acer                     | AOA150                                           | 23       | 23       | 1       | [A1F1](<Notebook/Acer/AOA/AOA150/A1F127826B4B>)                                                         |
| Dell                     | G3 3590                                          | 25       | 40       | 2       | [6EEC](<Notebook/Dell/G3/G3 3590/6EECAFEDBDAA>)                                                         |
| ASUSTek Computer         | M51Vr                                            | 26       | 26       | 1       | [2CA1](<Notebook/ASUSTek Computer/M51/M51Vr/2CA13C5744C0>)                                              |
| ASUSTek Computer         | VivoBook_ASUSLaptop X712DAP_M712DA               | 27       | 27       | 1       | [7D6E](<Notebook/ASUSTek Computer/VivoBook_ASUSLaptop/VivoBook_ASUSLaptop X712DAP_M712DA/7D6E3EA4DE8B>) |
| Lenovo                   | ThinkPad X1 Carbon 5th 20HR0013AU                | 27       | 27       | 1       | [23FE](<Notebook/Lenovo/ThinkPad/ThinkPad X1 Carbon 5th 20HR0013AU/23FE4576231A>)                       |
| Lenovo                   | Yoga 2 13 20344                                  | 27       | 27       | 1       | [2D05](<Notebook/Lenovo/Yoga/Yoga 2 13 20344/2D05CB611F92>)                                             |
| Hewlett-Packard          | 255 G7 Notebook PC                               | 28.2     | 48.9     | 3       | [9FB9](<Notebook/Hewlett-Packard/255/255 G7 Notebook PC/9FB94774A82E>)                                  |
| ASUSTek Computer         | 1003HAG                                          | 29       | 29       | 1       | [DD9F](<Notebook/ASUSTek Computer/1003/1003HAG/DD9F4C90C07F>)                                           |
| Acer                     | Swift SF314-42                                   | 29       | 38.8     | 4       | [E7ED](<Notebook/Acer/Swift/Swift SF314-42/E7EDD9738EA6>)                                               |
| Dell                     | Inspiron 5559                                    | 29       | 40       | 2       | [35A7](<Notebook/Dell/Inspiron/Inspiron 5559/35A7C406512F>)                                             |
| Dell                     | Latitude 9420                                    | 29       | 29       | 1       | [8A77](<Notebook/Dell/Latitude/Latitude 9420/8A77983183EB>)                                             |
| Hewlett-Packard          | Notebook                                         | 29       | 52.6     | 19      | [83BA](<Notebook/Hewlett-Packard/Notebook/Notebook/83BA24EAB95F>)                                       |
| Lenovo                   | ThinkPad T14s Gen 1 20UJS2YE00                   | 29       | 29       | 1       | [6BF6](<Notebook/Lenovo/ThinkPad/ThinkPad T14s Gen 1 20UJS2YE00/6BF685031045>)                          |
| Lenovo                   | ThinkPad X1 Carbon Gen 9 20XWA003CD              | 29       | 29       | 1       | [C97D](<Notebook/Lenovo/ThinkPad/ThinkPad X1 Carbon Gen 9 20XWA003CD/C97DAD0B7C4A>)                     |
| ASUSTek Computer         | 1005HA                                           | 30       | 35       | 3       | [48EA](<Notebook/ASUSTek Computer/1005/1005HA/48EABC5CC1E4>)                                            |
| Dell                     | Inspiron 5415                                    | 30       | 30       | 1       | [7AD3](<Notebook/Dell/Inspiron/Inspiron 5415/7AD3F8DF8A48>)                                             |
| Dell                     | Latitude 5410                                    | 30       | 51       | 3       | [1322](<Notebook/Dell/Latitude/Latitude 5410/1322FF431D0D>)                                             |
| Dell                     | Latitude E5500                                   | 30       | 30       | 1       | [188C](<Notebook/Dell/Latitude/Latitude E5500/188C293B5961>)                                            |
| MSI                      | MS-N033                                          | 30       | 30       | 1       | [6F6D](<Notebook/MSI/MS-N/MS-N033/6F6DB7763FE8>)                                                        |
| ASUSTek Computer         | X555DG                                           | 31       | 31       | 1       | [07E6](<Notebook/ASUSTek Computer/X555/X555DG/07E679168F3C>)                                            |
| Dell                     | Latitude D630                                    | 31       | 52.6     | 7       | [DA27](<Notebook/Dell/Latitude/Latitude D630/DA2731A13F2A>)                                             |
| Dell                     | Latitude X1                                      | 31       | 31       | 1       | [F0E4](<Notebook/Dell/Latitude/Latitude X1/F0E436063E50>)                                               |
| Hewlett-Packard          | EliteBook 820 G4                                 | 31       | 31       | 1       | [EC3A](<Notebook/Hewlett-Packard/EliteBook/EliteBook 820 G4/EC3A6C861A08>)                              |
| Hewlett-Packard          | Pavilion Gaming Laptop 15-ec1xxx                 | 31       | 47       | 6       | [0AE3](<Notebook/Hewlett-Packard/Pavilion/Pavilion Gaming Laptop 15-ec1xxx/0AE384DFB520>)               |
| Hewlett-Packard          | ProBook 440 G5                                   | 31       | 38       | 2       | [2324](<Notebook/Hewlett-Packard/ProBook/ProBook 440 G5/2324218651DC>)                                  |
| Lenovo                   | IdeaPad 330-17IKB 81DM                           | 31       | 43.3     | 3       | [0665](<Notebook/Lenovo/IdeaPad/IdeaPad 330-17IKB 81DM/06658F3565AB>)                                   |
| Lenovo                   | Legion S7 15IMH5 82BC                            | 31       | 31       | 1       | [5C92](<Notebook/Lenovo/Legion/Legion S7 15IMH5 82BC/5C92EB003D1A>)                                     |
| Lenovo                   | ThinkPad X260 20F5S08N00                         | 31       | 31       | 1       | [C45C](<Notebook/Lenovo/ThinkPad/ThinkPad X260 20F5S08N00/C45C42600D1F>)                                |
|                          |                                                  | 32       | 51       | 14      | [4EB4](<Notebook/Others/Others/Others/4EB4E9EF4527>)                                                    |
| ASUSTek Computer         | ZenBook UX431DA_UM431DA                          | 32       | 38.9     | 2       | [C136](<Notebook/ASUSTek Computer/ZenBook/ZenBook UX431DA_UM431DA/C136950321B2>)                        |
| Acer                     | AOA110                                           | 32       | 32       | 1       | [159F](<Notebook/Acer/AOA/AOA110/159F5C496DCA>)                                                         |
| Dell                     | Precision 5510                                   | 32       | 47.5     | 2       | [FA2A](<Notebook/Dell/Precision/Precision 5510/FA2A4AE7E130>)                                           |
| Hewlett-Packard          | 255 G5                                           | 32       | 32.2     | 2       | [E7BF](<Notebook/Hewlett-Packard/255/255 G5/E7BFD1A867DA>)                                              |
| Hewlett-Packard          | ProBook 445 G7                                   | 32       | 49       | 2       | [D601](<Notebook/Hewlett-Packard/ProBook/ProBook 445 G7/D601708086D2>)                                  |
| Hewlett-Packard          | ProBook 450 G7                                   | 32       | 44.8     | 6       | [F9EC](<Notebook/Hewlett-Packard/ProBook/ProBook 450 G7/F9EC70B43410>)                                  |
| Hewlett-Packard          | ProBook 455 G7                                   | 32       | 51.5     | 2       | [7623](<Notebook/Hewlett-Packard/ProBook/ProBook 455 G7/76239117B878>)                                  |
| IBM                      | ThinkPad T42 2373V4F                             | 32       | 32       | 1       | [BC33](<Notebook/IBM/ThinkPad/ThinkPad T42 2373V4F/BC33A7026E69>)                                       |
| LG Electronics           | 17Z990-R.AAS8U1                                  | 32       | 32       | 1       | [8365](<Notebook/LG Electronics/17Z990/17Z990-R.AAS8U1/83654FFF43B7>)                                   |
| Lenovo                   | ThinkBook 14p Gen 2 20YN                         | 32       | 32       | 1       | [C1F9](<Notebook/Lenovo/ThinkBook/ThinkBook 14p Gen 2 20YN/C1F9EEDFBD4F>)                               |
| Lenovo                   | ThinkPad E15 20RD001SUE                          | 32       | 32       | 1       | [174D](<Notebook/Lenovo/ThinkPad/ThinkPad E15 20RD001SUE/174D7DECE26D>)                                 |
| Lenovo                   | ThinkPad L14 Gen 2a 20X50049GE                   | 32       | 32       | 1       | [08B3](<Notebook/Lenovo/ThinkPad/ThinkPad L14 Gen 2a 20X50049GE/08B34FA434DF>)                          |
| TUXEDO                   | Pulse 15 Gen1                                    | 32.9     | 34.9     | 4       | [893A](<Notebook/TUXEDO/Pulse/Pulse 15 Gen1/893A8909092E>)                                              |
| ASUSTek Computer         | N501VW                                           | 33       | 51.5     | 2       | [A16F](<Notebook/ASUSTek Computer/N501/N501VW/A16F12062715>)                                            |
| Dell                     | Latitude 7390                                    | 33       | 57       | 4       | [2579](<Notebook/Dell/Latitude/Latitude 7390/2579220C9A21>)                                             |
| Hewlett-Packard          | 255 G6 Notebook PC                               | 33       | 39.5     | 4       | [02D1](<Notebook/Hewlett-Packard/255/255 G6 Notebook PC/02D150D98B9D>)                                  |
| Hewlett-Packard          | OMEN Laptop 15-en0xxx                            | 33       | 51       | 2       | [F1A0](<Notebook/Hewlett-Packard/OMEN/OMEN Laptop 15-en0xxx/F1A09A7F9BB8>)                              |

...

See all reports in the [Notebook](<Notebook>) directory.

Convertibles
------------

| MFG                      | Model                                            | Min Temp | Avg Temp | Samples | HWID                                                                                                        |
|--------------------------|--------------------------------------------------|----------|----------|---------|-------------------------------------------------------------------------------------------------------------|
| Hewlett-Packard          | ENVY x360 Convertible 15-ee0xxx                  | 26.8     | 26.8     | 1       | [8953](<Convertible/Hewlett-Packard/ENVY/ENVY x360 Convertible 15-ee0xxx/8953C870261F>)                     |
| Hewlett-Packard          | ProBook x360 435 G7                              | 29.2     | 29.2     | 1       | [5C86](<Convertible/Hewlett-Packard/ProBook/ProBook x360 435 G7/5C86FAF93611>)                              |
| ASUSTek Computer         | Q324UAK                                          | 30       | 30       | 1       | [5E32](<Convertible/ASUSTek Computer/Q324/Q324UAK/5E32DA62BC59>)                                            |
| Hewlett-Packard          | ENVY x360 Convertible 13-ay0xxx                  | 31       | 31       | 1       | [97E6](<Convertible/Hewlett-Packard/ENVY/ENVY x360 Convertible 13-ay0xxx/97E6CA0E6CA1>)                     |
| Samsung Electronics      | 950QDB                                           | 33       | 33       | 1       | [A732](<Convertible/Samsung Electronics/950/950QDB/A732D2F39173>)                                           |
| Hewlett-Packard          | ENVY x360 Convertible 15-ed0xxx                  | 34       | 34       | 1       | [99E4](<Convertible/Hewlett-Packard/ENVY/ENVY x360 Convertible 15-ed0xxx/99E4FD0403FA>)                     |
| ASUSTek Computer         | Q325UAR                                          | 37       | 37       | 1       | [BCF0](<Convertible/ASUSTek Computer/Q325/Q325UAR/BCF0D87D4DE0>)                                            |
| Hewlett-Packard          | EliteBook x360 830 G6                            | 37       | 37       | 1       | [D706](<Convertible/Hewlett-Packard/EliteBook/EliteBook x360 830 G6/D706B52ADE71>)                          |
| Lenovo                   | IdeaPad Flex 5 14ARE05 81X2                      | 37       | 53.7     | 3       | [74AA](<Convertible/Lenovo/IdeaPadFlex/IdeaPadFlex 5 14ARE05 81X2/74AAF9651496>)                            |
| Hewlett-Packard          | Pavilion x360 Convertible 14m-ba0xx              | 38       | 49.5     | 2       | [8F8E](<Convertible/Hewlett-Packard/Pavilion/Pavilion x360 Convertible 14m-ba0xx/8F8E6BEA650D>)             |
| Lenovo                   | ThinkPad X1 Yoga 2nd 20JES5033F                  | 38       | 38       | 1       | [0727](<Convertible/Lenovo/ThinkPad/ThinkPad X1 Yoga 2nd 20JES5033F/0727C01586E3>)                          |
| Lenovo                   | ThinkPad X13 Yoga Gen 2 20W8002GCD               | 39       | 39       | 1       | [AA2D](<Convertible/Lenovo/ThinkPad/ThinkPad X13 Yoga Gen 2 20W8002GCD/AA2D0076557D>)                       |
| Dell                     | Latitude 9420                                    | 40       | 61       | 2       | [6CF3](<Convertible/Dell/Latitude/Latitude 9420/6CF3D6E91245>)                                              |
| Dell                     | XPS 13 9310 2-in-1                               | 40       | 40       | 1       | [4F00](<Convertible/Dell/XPS/XPS 13 9310 2-in-1/4F0002F3D524>)                                              |
| Lenovo                   | Yoga 7 14ITL5 82BH                               | 40       | 51.6     | 5       | [B47A](<Convertible/Lenovo/Yoga/Yoga 7 14ITL5 82BH/B47AA262590B>)                                           |
| ASUSTek Computer         | VivoBook_ASUSLaptop TP412FA_TP412FA              | 41       | 41       | 1       | [3971](<Convertible/ASUSTek Computer/VivoBook_ASUSLaptop/VivoBook_ASUSLaptop TP412FA_TP412FA/3971D8CC78D9>) |
| Lenovo                   | IdeaPad FLEX 5-1570 81CA                         | 41       | 41       | 1       | [48C3](<Convertible/Lenovo/IdeaPadFlex/IdeaPadFlex 5-1570 81CA/48C3EAA7204E>)                               |
| Dell                     | Inspiron 7306 2n1                                | 42       | 54.6     | 9       | [9074](<Convertible/Dell/Inspiron/Inspiron 7306 2n1/90747104A1AD>)                                          |
| Hewlett-Packard          | ENVY x360 Convertible 15-bq1xx                   | 42       | 48       | 2       | [EBDB](<Convertible/Hewlett-Packard/ENVY/ENVY x360 Convertible 15-bq1xx/EBDBE6E11DFB>)                      |
| Lenovo                   | IdeaPad Flex 5 14ITL05 82HS                      | 42       | 42       | 1       | [E283](<Convertible/Lenovo/IdeaPadFlex/IdeaPadFlex 5 14ITL05 82HS/E283D7B91FE6>)                            |
| Lenovo                   | IdeaPad Flex 5 15ITL05 82HT                      | 42       | 42       | 1       | [E77B](<Convertible/Lenovo/IdeaPadFlex/IdeaPadFlex 5 15ITL05 82HT/E77B828FEEC5>)                            |
| Lenovo                   | IdeaPadFlex 5 14ITL05 82LT                       | 42       | 42       | 1       | [816B](<Convertible/Lenovo/IdeaPadFlex/IdeaPadFlex 5 14ITL05 82LT/816B018B0D52>)                            |
| Lenovo                   | ThinkPad Yoga 260 20FD0004US                     | 42       | 42       | 1       | [260A](<Convertible/Lenovo/ThinkPad/ThinkPad Yoga 260 20FD0004US/260AB44D4E87>)                             |
| Teclast                  | F5                                               | 42       | 50.3     | 3       | [FAF2](<Convertible/Teclast/F/F5/FAF229749873>)                                                             |
| Lenovo                   | Yoga 310-11IAP 80U2                              | 43       | 46       | 2       | [4954](<Convertible/Lenovo/Yoga/Yoga 310-11IAP 80U2/4954912FD789>)                                          |
| Lenovo                   | Yoga C740-14IML 81TC                             | 43       | 43       | 1       | [A7E7](<Convertible/Lenovo/Yoga/Yoga C740-14IML 81TC/A7E748E39915>)                                         |
| Acer                     | Aspire R5-571TG                                  | 44       | 44       | 1       | [6336](<Convertible/Acer/Aspire/Aspire R5-571TG/6336A87F3A5D>)                                              |
| Dell                     | Latitude 7390 2-in-1                             | 44       | 44       | 1       | [C9B7](<Convertible/Dell/Latitude/Latitude 7390 2-in-1/C9B781945AC4>)                                       |
| Dell                     | Latitude 7420                                    | 44       | 44.5     | 2       | [ED3C](<Convertible/Dell/Latitude/Latitude 7420/ED3CAFF8C282>)                                              |
| Hewlett-Packard          | ENVY x360 Convertible 15-dr1xxx                  | 44       | 44       | 1       | [1752](<Convertible/Hewlett-Packard/ENVY/ENVY x360 Convertible 15-dr1xxx/175288612806>)                     |
| Lenovo                   | ThinkPad L13 Yoga Gen 2 20VK0013MH               | 44       | 44       | 1       | [428B](<Convertible/Lenovo/ThinkPad/ThinkPad L13 Yoga Gen 2 20VK0013MH/428B81773F03>)                       |
| Lenovo                   | Yoga 6 13ARE05 82FN                              | 44       | 44       | 1       | [A18E](<Convertible/Lenovo/Yoga/Yoga 6 13ARE05 82FN/A18EAFD08CB4>)                                          |
| Hewlett-Packard          | ENVY x360 Convertible 15m-eu0xxx                 | 45.2     | 45.2     | 1       | [6F14](<Convertible/Hewlett-Packard/ENVY/ENVY x360 Convertible 15m-eu0xxx/6F14974A3E3A>)                    |
| ASUSTek Computer         | ZenBook UX562FAC                                 | 46       | 46       | 1       | [5861](<Convertible/ASUSTek Computer/ZenBook/ZenBook UX562FAC/5861A43F9A62>)                                |
| Hewlett-Packard          | EliteBook x360 1040 G8 Notebook PC               | 46       | 46       | 1       | [796D](<Convertible/Hewlett-Packard/EliteBook/EliteBook x360 1040 G8 Notebook PC/796D42BECDEC>)             |
| Lenovo                   | IdeaPad Flex 5 14ALC05 82HU                      | 46       | 46       | 1       | [2C54](<Convertible/Lenovo/IdeaPadFlex/IdeaPadFlex 5 14ALC05 82HU/2C5421DB8E39>)                            |
| Lenovo                   | IdeaPadFlex 4-1470 80SA                          | 46       | 46       | 1       | [4446](<Convertible/Lenovo/IdeaPadFlex/IdeaPadFlex 4-1470 80SA/4446EFA1913D>)                               |
| Lenovo                   | Yoga 510-14ISK 80S7                              | 46       | 46       | 1       | [27CA](<Convertible/Lenovo/Yoga/Yoga 510-14ISK 80S7/27CA2E3BFB4F>)                                          |
| TrekStor                 | Primebook C11                                    | 46       | 46       | 1       | [0D0E](<Convertible/TrekStor/Primebook/Primebook C11/0D0E083938F1>)                                         |
| Hewlett-Packard          | ENVY x360 Convertible 15-cp0xxx                  | 46.6     | 46.9     | 2       | [217E](<Convertible/Hewlett-Packard/ENVY/ENVY x360 Convertible 15-cp0xxx/217EE50D2173>)                     |
| Dell                     | Inspiron 7415 2-in-1                             | 47       | 47       | 1       | [4FF3](<Convertible/Dell/Inspiron/Inspiron 7415 2-in-1/4FF3B4F47781>)                                       |
| Medion                   | E2292                                            | 47       | 48       | 2       | [7719](<Convertible/Medion/E/E2292/7719D3111741>)                                                           |
| Dell                     | Latitude 3190 2-in-1                             | 48       | 48       | 1       | [8E5A](<Convertible/Dell/Latitude/Latitude 3190 2-in-1/8E5ACAA44DF8>)                                       |
| Hewlett-Packard          | ENVY x360 m6 Convertible                         | 48       | 48       | 1       | [AC66](<Convertible/Hewlett-Packard/ENVY/ENVY x360 m6 Convertible/AC669EFC2976>)                            |
| Hewlett-Packard          | EliteBook x360 1040 G6                           | 48       | 48       | 1       | [2CFD](<Convertible/Hewlett-Packard/EliteBook/EliteBook x360 1040 G6/2CFD58B2A932>)                         |
| Hewlett-Packard          | Pavilion x360 Convertible                        | 49       | 49       | 1       | [95A3](<Convertible/Hewlett-Packard/Pavilion/Pavilion x360 Convertible/95A382AFE1F2>)                       |
| Lenovo                   | ThinkPad L390 20NUS01W00                         | 49       | 49       | 1       | [699B](<Convertible/Lenovo/ThinkPad/ThinkPad L390 20NUS01W00/699B503B9811>)                                 |
| Lenovo                   | ThinkPad Yoga 370 20JJS01S1D                     | 49       | 49       | 1       | [AEA4](<Convertible/Lenovo/ThinkPad/ThinkPad Yoga 370 20JJS01S1D/AEA47047E644>)                             |
| Dell                     | Inspiron 7591 2n1                                | 51       | 51       | 1       | [E1AE](<Convertible/Dell/Inspiron/Inspiron 7591 2n1/E1AEE6A0BE67>)                                          |
| Hewlett-Packard          | EliteBook x360 1030 G2                           | 52       | 52       | 1       | [1683](<Convertible/Hewlett-Packard/EliteBook/EliteBook x360 1030 G2/168386F15558>)                         |
| Hewlett-Packard          | Pavilion x360 Convertible 14-cd0xxx              | 52       | 52       | 1       | [AFBB](<Convertible/Hewlett-Packard/Pavilion/Pavilion x360 Convertible 14-cd0xxx/AFBB7850A2F1>)             |

...

See all reports in the [Convertible](<Convertible>) directory.

Desktops
--------

| MFG                      | Model                                            | Min Temp | Avg Temp | Samples | HWID                                                                               |
|--------------------------|--------------------------------------------------|----------|----------|---------|------------------------------------------------------------------------------------|
| ASRock                   | FM2A88X Extreme6+                                | 0.6      | 15.6     | 3       | [5933](<Desktop/ASRock/FM2A88X/FM2A88X Extreme6+/5933BBB9876A>)                    |
| ASRock                   | 990FX Extreme3                                   | 1.9      | 1.9      | 1       | [9281](<Desktop/ASRock/990FX/990FX Extreme3/9281307ED05C>)                         |
| Hewlett-Packard          | 1850                                             | 2        | 13.7     | 7       | [C010](<Desktop/Hewlett-Packard/Compaq/Compaq Pro 6305 MT/C01015ACF3DD>)           |
| ASUSTek Computer         | F2A85-V PRO                                      | 3.2      | 3.2      | 1       | [A9E6](<Desktop/ASUSTek Computer/F2A85-V/F2A85-V PRO/A9E6D35D8906>)                |
| ASUSTek Computer         | F2A85-M PRO                                      | 4        | 4        | 1       | [90F3](<Desktop/ASUSTek Computer/F2A85-M/F2A85-M PRO/90F39DEEEF25>)                |
| Gigabyte Technology      | F2A88XM-D3HP                                     | 4.1      | 4.1      | 1       | [DA2B](<Desktop/Gigabyte Technology/F2/F2A88XM-D3HP/DA2BB2B1395B>)                 |
| ASRock                   | FM2A88X-ITX+                                     | 5        | 5        | 1       | [E6BE](<Desktop/ASRock/FM2/FM2A88X-ITX+/E6BE25BB0F5C>)                             |
| Intel                    | D510MO AAE76523-403                              | 5        | 5        | 1       | [7202](<Desktop/Intel/D510MO/D510MO AAE76523-403/72027B2F3221>)                    |
| ASRock                   | AM1H-ITX                                         | 5.4      | 22.5     | 3       | [BCC5](<Desktop/ASRock/AM1/AM1H-ITX/BCC594A85A48>)                                 |
| Hewlett-Packard          | 2B17                                             | 6.9      | 7.9      | 2       | [E355](<Desktop/Hewlett-Packard/500/500-333nr/E355CD30648C>)                       |
| ASRock                   | AD525PV3                                         | 7        | 7        | 1       | [2811](<Desktop/ASRock/AD525/AD525PV3/28111080211E>)                               |
| Gigabyte Technology      | F2A55M-S1                                        | 7        | 7        | 1       | [C44B](<Desktop/Gigabyte Technology/F2/F2A55M-S1/C44BB4D506D2>)                    |
| MSI                      | 2AE0                                             | 7        | 11.9     | 3       | [19F8](<Desktop/Hewlett-Packard/Pro/Pro 3515 Series/19F8E9BDC980>)                 |
| ASRock                   | 970M Pro3                                        | 7.4      | 15.1     | 4       | [6758](<Desktop/ASRock/970M/970M Pro3/67581F46D8B7>)                               |
| MSI                      | A88XM-E35                                        | 7.4      | 7.4      | 1       | [E0AD](<Desktop/MSI/MS/MS-7721/E0AD0181024B>)                                      |
| MSI                      | FM2-A55M-E33                                     | 7.4      | 8.7      | 2       | [3298](<Desktop/MSI/MS/MS-7721/3298264C8D8F>)                                      |
| ASRock                   | FM2A75M Pro4+                                    | 7.8      | 10.3     | 3       | [7949](<Desktop/ASRock/FM2A75M/FM2A75M Pro4+/79493E20CF5B>)                        |
| Gigabyte Technology      | F2A68HM-DS2                                      | 8        | 8        | 1       | [4571](<Desktop/Gigabyte Technology/F2/F2A68HM-DS2/457104BD717E>)                  |
| Gigabyte Technology      | GA-78LMT-USB3 x.x                                | 9.1      | 38       | 2       | [8FB2](<Desktop/Gigabyte Technology/GA-78LMT-USB3/GA-78LMT-USB3 6.0/8FB2EDFEB45F>) |
| ASRock                   | A75M-HVS                                         | 9.8      | 9.8      | 1       | [20A7](<Desktop/ASRock/A75/A75M-HVS/20A794106D60>)                                 |
| MSI                      | 970 GAMING                                       | 10       | 19.3     | 3       | [393C](<Desktop/MSI/MS/MS-7693/393C9613C3DB>)                                      |
| ASUSTek Computer         | A78M-A                                           | 11       | 11       | 1       | [C2CA](<Desktop/ASUSTek Computer/A78/A78M-A/C2CA3C5A12C9>)                         |
| Gigabyte Technology      | F2A78M-DS2                                       | 11.5     | 11.5     | 1       | [D12E](<Desktop/Gigabyte Technology/F2/F2A78M-DS2/D12E8AB84D9D>)                   |
| Gigabyte Technology      | F2A85XM-D3H                                      | 11.5     | 11.5     | 1       | [8460](<Desktop/Gigabyte Technology/F2/F2A85XM-D3H/846071FB1C93>)                  |
| Gigabyte Technology      | 990FXA-UD5                                       | 12.1     | 26.6     | 2       | [151B](<Desktop/Gigabyte Technology/990/990FXA-UD5/151BADA469E8>)                  |
| ASRock                   | N68-S3 FX                                        | 12.2     | 30.7     | 3       | [DEEC](<Desktop/ASRock/N68-S3/N68-S3 FX/DEEC5F68B516>)                             |
| ASUSTek Computer         | A68HM-K                                          | 12.6     | 12.6     | 1       | [5F11](<Desktop/ASUSTek Computer/A68/A68HM-K/5F11DCF53199>)                        |
| Foxconn                  | 2AB1                                             | 12.6     | 19.1     | 3       | [CE62](<Desktop/Hewlett-Packard/p6610/p6610f/CE62AE89AA3C>)                        |
| Gigabyte Technology      | GA-78LMT-USB3                                    | 12.6     | 13.1     | 2       | [CFAA](<Desktop/Gigabyte Technology/GA-78/GA-78LMT-USB3/CFAA36DFACD9>)             |
| ASRock                   | AM1B-ITX                                         | 13       | 18       | 2       | [9D31](<Desktop/ASRock/AM1/AM1B-ITX/9D312E03D2D1>)                                 |
| ASUSTek Computer         | M5A97 EVO R2.0                                   | 13       | 21.8     | 5       | [8283](<Desktop/ASUSTek Computer/M5A97/M5A97 EVO R2.0/828379E7FB76>)               |
| ASUSTek Computer         | A68HM-PLUS                                       | 14       | 18.9     | 2       | [0766](<Desktop/ASUSTek Computer/A68/A68HM-PLUS/07666656D364>)                     |
| ASUSTek Computer         | M11BB                                            | 14       | 14       | 1       | [CC80](<Desktop/ASUSTek Computer/M11/M11BB/CC8009830D2C>)                          |
| AMD                      | 970A-D3                                          | 14.8     | 14.8     | 1       | [054A](<Desktop/AMD/970/970A-D3/054A565E7DDC>)                                     |
| Colorful Technology      | C.A68M-K PLUS V16                                | 14.8     | 14.8     | 1       | [7D7F](<Desktop/Colorful Technology/C.A68M-K/C.A68M-K PLUS V16/7D7F16D4BDAB>)      |
| ASUSTek Computer         | 970 PRO GAMING/AURA                              | 14.9     | 20.8     | 2       | [C480](<Desktop/ASUSTek Computer/970/970 PRO GAMING-AURA/C480F0029D6F>)            |
| ASUSTek Computer         | A88X-PLUS/USB                                    | 14.9     | 14.9     | 1       | [473C](<Desktop/ASUSTek Computer/A88/A88X-PLUS-USB/473CF2FA2B81>)                  |
| ASRock                   | 960GM/U3S3 FX                                    | 15.1     | 15.1     | 1       | [7E1E](<Desktop/ASRock/960GM-U3S3/960GM-U3S3 FX/7E1EBF4E07FC>)                     |
| Gigabyte Technology      | M68MT-D3P                                        | 15.4     | 17.2     | 2       | [9452](<Desktop/Brunen IT/M68/M68MT-D3P/94521C83B94C>)                             |
| ASRock                   | 970A-G                                           | 16       | 21       | 2       | [7712](<Desktop/ASRock/970/970A-G/771233B45648>)                                   |
| Gigabyte Technology      | A320M-S2H-CF                                     | 16       | 27.4     | 9       | [DEB8](<Desktop/Gigabyte Technology/A320/A320M-S2H/DEB8F8B66895>)                  |
| Gigabyte Technology      | A520M H                                          | 16       | 16       | 1       | [9194](<Desktop/Gigabyte Technology/A520M/A520M H/9194124CC94F>)                   |
| Gigabyte Technology      | AB350M-DS3H V2-CF                                | 16       | 16       | 1       | [0A69](<Desktop/Gigabyte Technology/AB350M-DS3H/AB350M-DS3H V2/0A6971558E1C>)      |
| Gigabyte Technology      | B450M AORUS ELITE                                | 16       | 16       | 1       | [7D0D](<Desktop/Gigabyte Technology/B450M/B450M AORUS ELITE/7D0DD77BF926>)         |
| Gigabyte Technology      | B450M DS3H V2                                    | 16       | 16.3     | 3       | [CBCA](<Desktop/Gigabyte Technology/B450M/B450M DS3H V2/CBCAC94781B6>)             |
| Gigabyte Technology      | B450M DS3H-CF                                    | 16       | 22.2     | 15      | [A36B](<Desktop/Gigabyte Technology/B450M/B450M DS3H/A36BC0E4BE59>)                |
| Gigabyte Technology      | B450M S2H                                        | 16       | 30.2     | 5       | [6E93](<Desktop/Gigabyte Technology/B450M/B450M S2H/6E93F11413FE>)                 |
| Gigabyte Technology      | B550 AORUS ELITE V2                              | 16       | 33       | 2       | [FF0B](<Desktop/Gigabyte Technology/B550/B550 AORUS ELITE V2/FF0BA3A27009>)        |
| Gigabyte Technology      | B550M AORUS ELITE                                | 16       | 36.9     | 4       | [5596](<Desktop/Gigabyte Technology/B550M/B550M AORUS ELITE/55969AB0CCE0>)         |
| Gigabyte Technology      | X570 AORUS ELITE                                 | 16       | 22.9     | 5       | [931A](<Desktop/Gigabyte Technology/X570/X570 AORUS ELITE/931A6235F0D9>)           |
| Gigabyte Technology      | X570 AORUS ELITE WIFI                            | 16       | 23.3     | 3       | [5DD0](<Desktop/Gigabyte Technology/X570/X570 AORUS ELITE WIFI/5DD09FB03DAD>)      |

...

See all reports in the [Desktop](<Desktop>) directory.

All In Ones
-----------

| MFG                      | Model                                            | Min Temp | Avg Temp | Samples | HWID                                                                                       |
|--------------------------|--------------------------------------------------|----------|----------|---------|--------------------------------------------------------------------------------------------|
| Acidanthera              | Mac-AA95B1DDAB278B95 iMac19,1                    | 20       | 26       | 2       | [2E97](<All In One/Acidanthera/iMac19/iMac19,1/2E9792DAB2C5>)                              |
| Hewlett-Packard          | 84EE 1100                                        | 34       | 37       | 2       | [AFF0](<All In One/Hewlett-Packard/Pavilion/Pavilion All-in-One 24-xa0xxx/AFF07EB65B70>)   |
| Acidanthera              | Mac-81E3E92DD6088272 iMac14,4                    | 35       | 35       | 1       | [E2C6](<All In One/Acidanthera/iMac14/iMac14,4/E2C69985CC45>)                              |
| Dell                     | 0YGR09 A00                                       | 37.5     | 37.5     | 1       | [44B0](<All In One/Dell/Inspiron/Inspiron 24 5475/44B04AE381C1>)                           |
| iRU                      | AraT                                             | 38       | 38       | 1       | [9007](<All In One/iRU/A/A2313/9007113D5706>)                                              |
| Lenovo                   | Larne CRB 31900058 WIN 2586190841099             | 40.1     | 40.1     | 1       | [3ED6](<All In One/Lenovo/C40-05/C40-05 F0B5004EUK/3ED6D2F5B3CF>)                          |
| Dell                     | 0CRWCR A01                                       | 41       | 43       | 2       | [4E45](<All In One/Dell/OptiPlex/OptiPlex 9010 AIO/4E45215C7846>)                          |
| Hewlett-Packard          | 2B42                                             | 41       | 41       | 1       | [412A](<All In One/Hewlett-Packard/23/23-q014a/412A4AE1BE51>)                              |
| Hewlett-Packard          | 3395                                             | 41       | 41       | 1       | [AF59](<All In One/Hewlett-Packard/Compaq/Compaq 8200 Elite AiO Business PC/AF5919835808>) |
| Hewlett-Packard          | 2B0D A01                                         | 42       | 42       | 1       | [CEF5](<All In One/Hewlett-Packard/23/23-p010ns/CEF595524FE0>)                             |
| Hewlett-Packard          | 82F3 A01                                         | 42       | 42       | 1       | [BB85](<All In One/Hewlett-Packard/ENVY/ENVY 27-b114/BB85DDA47130>)                        |
| Apple                    | Mac-F4218EC8 DVT                                 | 43       | 43       | 1       | [4D2A](<All In One/Apple/iMac5/iMac5,2/4D2ADA87ED95>)                                      |
| Hewlett-Packard          | 2B0C MVB,A                                       | 43       | 43       | 1       | [DAE8](<All In One/Hewlett-Packard/18/18-4121la/DAE88CA37FAB>)                             |
| Hewlett-Packard          | 86C7                                             | 43       | 43       | 1       | [AC2C](<All In One/Hewlett-Packard/ENVY/ENVY All-in-One 32-a11xxx/AC2CB5057523>)           |
| Apple                    | Mac-BE088AF8C5EB4FA2 iMac18,3                    | 44       | 51.5     | 2       | [91F9](<All In One/Apple/iMac18/iMac18,3/91F91D01709A>)                                    |
| Apple                    | Mac-FFE5EF870D7BA81A iMac16,2                    | 44       | 44       | 1       | [224B](<All In One/Apple/iMac16/iMac16,2/224B9F366AD7>)                                    |
| Dell                     | 0D90HM A00                                       | 44       | 44       | 1       | [D5FA](<All In One/Dell/Inspiron/Inspiron 24-3459/D5FAD644929A>)                           |
| Lenovo                   | 7745                                             | 44       | 44       | 1       | [71BD](<All In One/Lenovo/IdeaCentre/IdeaCentre B520 7745/71BD816847A0>)                   |
| Medion                   | BTDD-EAIO                                        | 45       | 45       | 1       | [EAE7](<All In One/Medion/V/V20/EAE703BD7B5E>)                                             |
| Acer                     | Veriton Z4710G                                   | 46       | 46       | 1       | [B483](<All In One/Acer/Veriton/Veriton Z4710G/B48302FD59AB>)                              |
| Dell                     | Inspiron One 2320                                | 46       | 46       | 1       | [6112](<All In One/Dell/Inspiron/Inspiron One 2320/61122D664D9D>)                          |
| Lenovo                   | 3729 No DPK                                      | 46       | 46       | 1       | [43AF](<All In One/Lenovo/IdeaCentre/IdeaCentre A340-24IGM F0E7003PRK/43AFE663F493>)       |
| Apple                    | Mac-AA95B1DDAB278B95 iMac19,1                    | 48       | 54       | 2       | [3303](<All In One/Apple/iMac19/iMac19,1/330392FCD8B9>)                                    |
| Apple                    | Mac-F2218FA9                                     | 48       | 54       | 2       | [4F99](<All In One/Apple/iMac9/iMac9,1/4F9940E6FFA2>)                                      |
| ASUSTek Computer         | V241EA                                           | 50       | 50       | 1       | [7F3E](<All In One/ASUSTek Computer/ASUS/ASUS Vivo AIO V241EA_V241EA/7F3E7AF4C408>)        |
| Dell                     | 0HD5K4 A00                                       | 50       | 50       | 1       | [4A39](<All In One/Dell/Inspiron/Inspiron 20 Model 3048/4A397B580265>)                     |
| Acer                     | Aspire C24-963                                   | 51       | 51       | 1       | [CC4A](<All In One/Acer/Aspire/Aspire C24-963/CC4A926A8703>)                               |
| Apple                    | Mac-942B59F58194171B iMac12,2                    | 51       | 53       | 2       | [4231](<All In One/Apple/iMac12/iMac12,2/4231266DB880>)                                    |
| Samsung Electronics      | DP505A2G-K01CH SAMSUNG_SW_REVISION_1234567890... | 51       | 51       | 1       | [F302](<All In One/Samsung Electronics/DP505/DP505-DM515/F30224D89080>)                    |
| Apple                    | Mac-42FD25EABCABB274 iMac15,1                    | 52       | 52       | 1       | [5F6A](<All In One/Apple/iMac15/iMac15,1/5F6A7F51EB9F>)                                    |
| Hewlett-Packard          | 838B 0100                                        | 53       | 53       | 1       | [975B](<All In One/Hewlett-Packard/838B/838B 0100/975B05E0D412>)                           |
| Apple                    | Mac-FC02E91DDD3FA6A4 iMac13,2                    | 54       | 54       | 1       | [767B](<All In One/Apple/iMac13/iMac13,2/767BA25290CE>)                                    |
| Dell                     | 0KFKMF A00                                       | 54.4     | 54.4     | 1       | [06DB](<All In One/Dell/Inspiron/Inspiron 27 7775/06DB8E8A2E7C>)                           |
| Apple                    | Mac-63001698E7A34814 iMac19,2                    | 55       | 55       | 1       | [A8DC](<All In One/Apple/iMac19/iMac19,2/A8DC69AC1EB8>)                                    |
| Apple                    | Mac-F2238BAE iMac11,3                            | 55       | 55       | 1       | [7C0A](<All In One/Apple/iMac11/iMac11,3/7C0A72B3FD5F>)                                    |
| Hewlett-Packard          | 86F0 11000                                       | 55       | 55       | 1       | [20A2](<All In One/Hewlett-Packard/All-in-One/All-in-One 24-df0xxx/20A2CA3001FD>)          |
| Apple                    | Mac-F227BEC8 PVT                                 | 56       | 56       | 1       | [E67E](<All In One/Apple/iMac8/iMac8,1/E67EA67ADB59>)                                      |
| Apple                    | Mac-27ADBB7B4CEE8E61 iMac14,2                    | 58       | 58       | 1       | [0E5C](<All In One/Apple/iMac14/iMac14,2/0E5CBE0A407A>)                                    |
| Hewlett-Packard          | 8381 1000                                        | 58.4     | 58.4     | 1       | [4F2D](<All In One/Hewlett-Packard/All-in-One/All-in-One/4F2DDEA20F44>)                    |
| Apple                    | Mac-77F17D7DA9285301 iMac18,2                    | 60       | 60.5     | 2       | [044A](<All In One/Apple/iMac18/iMac18,2/044A5BCEA282>)                                    |
| Dell                     | 05R2TK A01                                       | 61       | 61       | 1       | [22BA](<All In One/Dell/XPS/XPS 2720/22BAEC305099>)                                        |
| Apple                    | Mac-B809C3757DA9BB8D iMac17,1                    | 62       | 62       | 1       | [FF4F](<All In One/Apple/iMac17/iMac17,1/FF4F055D8277>)                                    |
| Hewlett-Packard          | 2B3B                                             | 62.1     | 62.1     | 1       | [6300](<All In One/Hewlett-Packard/20/20-r118/6300AC4B470A>)                               |
| Hewlett-Packard          | 8245 001                                         | 63       | 63       | 1       | [323F](<All In One/Hewlett-Packard/24/24-g013ns/323FC2F05365>)                             |
| Acer                     | Aspire C22-963                                   | 65       | 65       | 1       | [AB39](<All In One/Acer/Aspire/Aspire C22-963/AB390A2C3656>)                               |
| MSI                      | MS-B0961                                         | 68       | 68       | 1       | [E735](<All In One/MSI/MS-B/MS-B09611/E735A240DD16>)                                       |
| ASUSTek Computer         | ET2010AG                                         | 71       | 71       | 1       | [03DD](<All In One/ASUSTek Computer/ET2010/ET2010AG/03DD34D94A32>)                         |
| Quanta                   | XV1                                              | 80       | 80       | 1       | [FCA4](<All In One/VIZIO/CA/CA27/FCA4878A3D40>)                                            |
| Gigabyte Technology      | MFH27AI-SI                                       | 87       | 87       | 1       | [AF86](<All In One/Wortmann AG/TERRA_PC/TERRA_PC/AF864A645B1E>)                            |

...

See all reports in the [All In One](<All In One>) directory.

Servers
-------

| MFG                      | Model                                            | Min Temp | Avg Temp | Samples | HWID                                                                              |
|--------------------------|--------------------------------------------------|----------|----------|---------|-----------------------------------------------------------------------------------|
| Dell                     | 0MD99X A07                                       | 28       | 28       | 1       | [6232](<Server/Dell/PowerEdge/PowerEdge R710/6232D14EDFD6>)                       |
| ASUSTek Computer         | WS-C621E-SAGE Series                             | 30       | 30       | 1       | [501A](<Server/ASUSTek Computer/WS-C621E-SAGE/WS-C621E-SAGE Series/501AB4AFC716>) |
| Sun Microsystems         | Sun Fire X2270 M2 375-3614-03                    | 30       | 30       | 1       | [772B](<Server/Sun Microsystems/Sun/Sun Fire X2270 M2/772B238D25B3>)              |
| Supermicro               | H8DCL                                            | 30.2     | 30.2     | 1       | [B428](<Server/Supermicro/H8/H8DCL/B4286ED9168E>)                                 |
| Dell                     | 01CTXG A07                                       | 31       | 31       | 1       | [7F13](<Server/Dell/PowerEdge/PowerEdge T710/7F137F927953>)                       |
| Intel                    | S1200SP H57532-350                               | 31       | 31       | 1       | [3308](<Server/Intel/S1200/S1200SP/330813FC534A>)                                 |
| Hewlett-Packard          | ProLiant DL360 Gen9                              | 32       | 32       | 1       | [74B9](<Server/Hewlett-Packard/ProLiant/ProLiant DL360 Gen9/74B986D09E54>)        |
| Dell                     | 081VG9 A05                                       | 34       | 34       | 1       | [B0AF](<Server/Dell/PowerEdge/PowerEdge T440/B0AF20400020>)                       |
| Dell                     | 0RN4PJ A01                                       | 34       | 34       | 1       | [B260](<Server/Dell/Precision/Precision 7920 Tower/B2600979C56F>)                 |
| Supermicro               | M11SDV-8CT-LN4F                                  | 34.5     | 34.5     | 1       | [7862](<Server/Supermicro/Super/Super Server/786209B7D1E0>)                       |
| ASUSTek Computer         | Z11PH-D12 Series                                 | 35       | 35       | 1       | [5DCD](<Server/ASUSTek Computer/RS720/RS720Q-E9-RS24-S/5DCDC5F18084>)             |
| Intel                    | S1200SP H57532-250                               | 35       | 35       | 1       | [97A3](<Server/Intel/S1200/S1200SP/97A3BEC6A314>)                                 |
| Dell                     | 0KX11M A04                                       | 36       | 36       | 1       | [DE89](<Server/Dell/PowerEdge/PowerEdge T430/DE8965731D6A>)                       |
| Dell                     | 0MX4YF A01                                       | 36       | 36       | 1       | [525E](<Server/Dell/PowerEdge/PowerEdge T620/525ECA602515>)                       |
| Huawei                   | BC11SPSCB0 V100R005                              | 36       | 36       | 1       | [9460](<Server/Huawei/2288H/2288H V5/94600F3C6456>)                               |
| Intel                    | S3210SH FRU Ver                                  | 36       | 36       | 1       | [B448](<Server/Intel/S3210/S3210SH/B448B56E8965>)                                 |
| Intel                    | S5520HC E26045-457                               | 37       | 37       | 1       | [5E89](<Server/Intel/S5520/S5520HC/5E89F6CD026C>)                                 |
| Oracle                   | ASSY,MOTHERBOARD,X4170 511-1213-07               | 37       | 37       | 1       | [306C](<Server/Oracle/Sun/Sun Fire X4270 M2 SERVER/306C153487E1>)                 |
| Supermicro               | X9DRi-LN4+/X9DR3-LN4+                            | 37       | 37       | 1       | [B8B0](<Server/Supermicro/X9/X9DRi-LN4+-X9DR3-LN4+/B8B03C82409C>)                 |
| Fujitsu                  | D2619 S26361-D2619-A14 WGS05 GS03                | 38       | 38       | 1       | [8D10](<Server/Fujitsu/PRIMERGY/PRIMERGY RX300 S5/8D101F548ADF>)                  |
| Fujitsu                  | D3031 S26361-D3031-A100-4 WGS02 G                | 38       | 38       | 1       | [A598](<Server/Fujitsu/PRIMERGY/PRIMERGY RX200 S6/A598FCA268BC>)                  |
| Supermicro               | X10SAE                                           | 39       | 51.7     | 3       | [6D8B](<Server/Supermicro/X10/X10SAE/6D8BBA911035>)                               |
| Hewlett-Packard          | ProLiant DL360p Gen8                             | 40       | 45.5     | 2       | [9DB7](<Server/Hewlett-Packard/ProLiant/ProLiant DL360p Gen8/9DB7F0CECFBF>)       |
| IBM                      | 00D4062                                          | 41       | 41       | 1       | [86B4](<Server/IBM/System/System x3650 M3 -[794522U]-/86B4DEEC0CA5>)              |
| Hewlett-Packard          | ProLiant DL380 G6                                | 42       | 42       | 1       | [B0C4](<Server/Hewlett-Packard/ProLiant/ProLiant DL380 G6/B0C4672C643E>)          |
| Supermicro               | X8DTH                                            | 42       | 42       | 1       | [0363](<Server/Supermicro/X8/X8DTH-i-6-iF-6F/03637143A526>)                       |
| HPE                      | ProLiant MicroServer Gen10 871721-001            | 42.1     | 42.1     | 1       | [7FD7](<Server/HPE/ProLiant/ProLiant MicroServer Gen10/7FD7445C9115>)             |
| ASUSTek Computer         | Z10PR-D16 Series                                 | 44       | 44       | 1       | [B939](<Server/ASUSTek Computer/RS400/RS400-E8-PS2-F/B939D9497146>)               |
| Supermicro               | X8DTL                                            | 45       | 45       | 1       | [4816](<Server/Supermicro/X8/X8DTL/4816D25892DF>)                                 |
| HPE                      | ProLiant DL360 Gen10                             | 46       | 46       | 1       | [4443](<Server/HPE/ProLiant/ProLiant DL360 Gen10/4443D3087423>)                   |
| Dell                     | 0R4CNN A01                                       | 46.5     | 46.5     | 1       | [9A7A](<Server/Dell/PowerEdge/PowerEdge R7515/9A7A286BD3E1>)                      |
| Supermicro               | X10DRS-3U                                        | 47       | 47       | 1       | [DE1D](<Server/Tegile/T4200/T4200-C1/DE1DF2E54E4C>)                               |
| Supermicro               | X8DT6                                            | 47       | 47       | 1       | [D4DB](<Server/Supermicro/X8/X8DT6/D4DB505493D1>)                                 |
| Hewlett-Packard          | ProLiant BL460c G6                               | 48       | 48       | 1       | [FA97](<Server/Hewlett-Packard/ProLiant/ProLiant BL460c G6/FA97A758D2C6>)         |
| Dell                     | 01V648 A03                                       | 49       | 49       | 1       | [C25F](<Server/Dell/PowerEdge/PowerEdge R410/C25F044F8862>)                       |
| Supermicro               | X9DRD-7LN4F                                      | 49       | 49       | 1       | [A67A](<Server/Supermicro/X9/X9DRD-7LN4F-X9DRD-EF/A67A7A3867A4>)                  |
| Dell                     | 0W7H8C A02                                       | 52       | 52       | 1       | [E6FA](<Server/Dell/PowerEdge/PowerEdge T320/E6FACFAD0BA2>)                       |
| Intel                    | S5000XVN                                         | 54       | 54       | 1       | [1870](<Server/Intel/S5000/S5000XVN/187007C30239>)                                |
| Dell                     | 051XDX A09                                       | 60       | 60       | 1       | [F12E](<Server/Dell/PowerEdge/PowerEdge R520/F12E87D98729>)                       |
| Supermicro               | X9DRD-iF                                         | 79       | 79       | 1       | [3AD7](<Server/SYNNEX/HYVE-ZEUS/HYVE-ZEUS/3AD7D5C11C7F>)                          |

...

See all reports in the [Server](<Server>) directory.

Mini Pcs
--------

| MFG                      | Model                                            | Min Temp | Avg Temp | Samples | HWID                                                                           |
|--------------------------|--------------------------------------------------|----------|----------|---------|--------------------------------------------------------------------------------|
| Radiant Systems          | P845                                             | 26       | 26       | 1       | [6CDD](<Mini Pc/Radiant Systems/P/P845/6CDD298F6645>)                          |
| Supermicro               | A1SRi                                            | 30       | 30       | 1       | [F097](<Mini Pc/Supermicro/A1/A1SAi/F097711C2215>)                             |
| ZOTAC                    | ZBOX-BI320                                       | 31       | 31       | 1       | [C92E](<Mini Pc/ZOTAC/ZBOX-BI/ZBOX-BI320/C92EA8DE9F21>)                        |
| Intel                    | NUC10i7FNB M38062-307                            | 34       | 34       | 1       | [59E6](<Mini Pc/Intel Client Systems/NUC10/NUC10i7FNK/59E65E4F0375>)           |
| Intel                    | NUC10i7FNB K61360-302                            | 36       | 51.4     | 17      | [BB89](<Mini Pc/Intel Client Systems/NUC10/NUC10i7FNH/BB89D4F8427B>)           |
| ASUSTek Computer         | PN62S                                            | 37       | 37       | 1       | [C5A8](<Mini Pc/ASUSTek Computer/MINIPC/MINIPC PN62S/C5A82ADBCB5E>)            |
| Intel                    | D425KT AAE93083-401                              | 37       | 37       | 1       | [10EE](<Mini Pc/Intel/D425KT/D425KT AAE93083-401/10EE8CFFD4E7>)                |
| Intel                    | NUC7JYB J67969-404                               | 37       | 37       | 1       | [80C9](<Mini Pc/Intel Client Systems/NUC7/NUC7PJYH/80C97F0A7DE7>)              |
| Lenovo                   | 3111 SDK0J40697 WIN 3305064661157                | 37       | 37       | 1       | [E23C](<Mini Pc/Lenovo/ThinkCentre/ThinkCentre M710q 10MR000XGE/E23CB6C3C2A6>) |
| Intel                    | NUC10i3FNB K61362-303                            | 38       | 46.7     | 6       | [5AED](<Mini Pc/Intel Client Systems/NUC10/NUC10i3FNH/5AEDA3C661BA>)           |
| Intel                    | NUC10i5FNB K61361-306                            | 40       | 43       | 2       | [D019](<Mini Pc/Intel Client Systems/NUC10/NUC10i5FNH/D0195477DF6A>)           |
| Intel                    | NUC11PABi5 K90634-302                            | 40       | 40       | 1       | [426F](<Mini Pc/Intel Client Systems/NUC11/NUC11PAKi5/426FE5EB182C>)           |
| Intel                    | NUC8BEB J72693-306                               | 40       | 40       | 1       | [6683](<Mini Pc/Intel Client Systems/NUC8/NUC8i3BEH/6683D67CDFB4>)             |
| ASUSTek Computer         | PN50                                             | 41       | 41       | 1       | [8F82](<Mini Pc/ASUSTek Computer/MINIPC/MINIPC PN50/8F8267FBEFAB>)             |
| CompuLab                 | fitlet2                                          | 41       | 62       | 2       | [5B24](<Mini Pc/CompuLab/fitlet/fitlet2/5B241EBA4461>)                         |
| Intel                    | NUC10i5FNB K61361-303                            | 41       | 47.1     | 7       | [761C](<Mini Pc/Intel Client Systems/NUC10/NUC10i5FNH/761C31944B95>)           |
| Intel                    | NUC10i5FNB K61361-305                            | 41       | 45.8     | 4       | [5E48](<Mini Pc/Intel Client Systems/NUC10/NUC10i5FNH/5E482D78FFB4>)           |
| Intel                    | NUC10i7FNB K61360-303                            | 41       | 49.2     | 9       | [B989](<Mini Pc/Intel Client Systems/NUC10/NUC10i7FNH/B9893BD86A34>)           |
| Intel                    | NUC10i7FNB K61360-306                            | 41       | 46.8     | 4       | [773A](<Mini Pc/Intel Client Systems/NUC10/NUC10i7FNH/773AB968BC22>)           |
| Lenovo                   | 3144                                             | 41       | 41       | 1       | [7F8C](<Mini Pc/Lenovo/ThinkCentre/ThinkCentre M630e 10YM001SRU/7F8C8151CE1E>) |
| Intel                    | NUC10i3FNB K61362-305                            | 42       | 47.5     | 2       | [8F91](<Mini Pc/Intel Client Systems/NUC10/NUC10i3FNK/8F917473016D>)           |
| Intel                    | NUC10i7FNB K61360-305                            | 42       | 47       | 4       | [44BE](<Mini Pc/System76/Meerkat/Meerkat/44BE3485BC0D>)                        |
| Intel                    | NUC5i3RYB H41000-504                             | 42       | 42       | 1       | [9171](<Mini Pc/Intel/NUC5i3RYB/NUC5i3RYB H41000-504/917162A30A82>)            |
| Lenovo                   | Aptio CRB NOK                                    | 42       | 42       | 1       | [934A](<Mini Pc/Lenovo/E50-00/E50-00 90BX0017IX/934A6C2A3776>)                 |
| MITSUSHIBA               | Intel NUC I3 BP-011F / BP-013F                   | 42       | 42       | 1       | [23F9](<Mini Pc/MITSUSHIBA/Intel/Intel NUC I3 BP-011F-BP-013F/23F9BF88A248>)   |
| Intel                    | NUC10i5FNB K61361-304                            | 43       | 43       | 1       | [EA79](<Mini Pc/Intel Client Systems/NUC10/NUC10i5FNH/EA798DD5FB57>)           |
| Intel                    | NUC10i7FNB K61360-304                            | 43       | 60.7     | 3       | [F056](<Mini Pc/System76/Meerkat/Meerkat/F05627FEB209>)                        |
| Intel                    | NUC11PHBi7 M26151-402                            | 44       | 44       | 1       | [04FC](<Mini Pc/Intel Client Systems/NUC11/NUC11PHi7/04FC335AEF4E>)            |
| Intel                    | NUC7JYB J67967-404                               | 44       | 44       | 1       | [6236](<Mini Pc/Intel Client Systems/NUC7/NUC7CJYH/6236CFFD80EB>)              |
| Intel                    | NUC8i7HVB J68196-504                             | 44       | 44       | 1       | [C707](<Mini Pc/Intel Client Systems/NUC8/NUC8i7HVK/C7070581668B>)             |
| Hewlett-Packard          | 8267 A01                                         | 44.8     | 44.8     | 1       | [2A38](<Mini Pc/Hewlett-Packard/t530/t530 Thin Client/2A38F450F1CB>)           |
| Intel                    | NUC10i3FNB K61362-302                            | 46       | 57.2     | 4       | [F804](<Mini Pc/Intel Client Systems/NUC10/NUC10i3FNK/F8049112DC4B>)           |
| ZOTAC                    | ZBOX-ID18                                        | 46       | 46       | 1       | [7F65](<Mini Pc/ZOTAC/ZBOX-ID/ZBOX-ID18/7F6546651FC3>)                         |
| AZW                      | U55                                              | 47       | 53       | 2       | [6E5C](<Mini Pc/AZW/U/U55/6E5CDFDCB8E8>)                                       |
| Apple                    | Mac-35C5E08120C7EEAF Macmini7,1                  | 47       | 51       | 2       | [8B7E](<Mini Pc/Apple/Macmini7/Macmini7,1/8B7E64B5D9F4>)                       |
| Intel                    | NUC8i7HVB J68196-502                             | 47       | 47       | 1       | [1D6B](<Mini Pc/Intel/NUC8/NUC8i7HVK/1D6B0701FDE5>)                            |
| ASUSTek Computer         | PN40                                             | 48       | 48       | 1       | [56D9](<Mini Pc/ASUSTek Computer/PN/PN40/56D952117318>)                        |
| Intel                    | NUC10i5FNB M38063-307                            | 48       | 48       | 1       | [C532](<Mini Pc/Intel Client Systems/NUC10/NUC10i5FNK/C532240BD7A5>)           |
| BESSTAR Tech             | GB7                                              | 49       | 49       | 1       | [BF8F](<Mini Pc/BESSTAR Tech/GK/GK50/BF8FB6EFA2A6>)                            |
| Intel                    | NUC5i3RYB H41000-507                             | 49       | 49       | 1       | [3075](<Mini Pc/Intel/NUC5i3RYB/NUC5i3RYB H41000-507/307528649A8F>)            |
| Intel                    | NUC8BEB J72693-307                               | 49       | 49       | 1       | [6BC5](<Mini Pc/Intel Client Systems/NUC8/NUC8i3BEH/6BC503379C45>)             |
| Intel                    | NUC9VXQNB K47179-402                             | 49       | 49       | 1       | [6985](<Mini Pc/Intel Client Systems/NUC9/NUC9VXQNX/698519E45C28>)             |
| AMI                      | Aptio CRB                                        | 50       | 55       | 2       | [4331](<Mini Pc/AMI/Aptio/Aptio CRB/4331D427A030>)                             |
| Intel                    | NUC6i7KYB H90766-406                             | 50       | 50       | 1       | [4BA8](<Mini Pc/Intel/NUC6i7KYB/NUC6i7KYB H90766-406/4BA861BB27B0>)            |
| Intel                    | NUC8BEB J72693-308                               | 50       | 50       | 1       | [D792](<Mini Pc/Intel Client Systems/NUC8/NUC8i3BEH/D792C715F492>)             |
| Intel                    | NUC10i7FNB M38062-306                            | 51       | 51       | 1       | [74FC](<Mini Pc/System76/Meerkat/Meerkat/74FCB788D4E3>)                        |
| BESSTAR Tech             | Cherry Trail CR                                  | 52       | 52       | 1       | [C9C4](<Mini Pc/BESSTAR Tech/Z83/Z83-F/C9C4A56D40BD>)                          |
| Intel                    | NUC6CAYB J23203-409                              | 52       | 52       | 1       | [6CC0](<Mini Pc/Intel Client Systems/NUC6/NUC6CAYH/6CC0B4F1BE95>)              |
| Intel                    | NUC6i7KYB H90766-404                             | 52       | 52       | 1       | [2916](<Mini Pc/Intel/NUC6i7KYB/NUC6i7KYB H90766-404/2916D8EA6A6D>)            |
| Intel                    | NUC11TNBi7 M11895-402                            | 53       | 53       | 1       | [4E60](<Mini Pc/Intel Client Systems/NUC11/NUC11TNBi7/4E60341F3B14>)           |
| Intel                    | NUC6i3SYB H81132-503                             | 53       | 53       | 1       | [B392](<Mini Pc/Intel/NUC6i3SYB/NUC6i3SYB H81132-503/B392B274D5C1>)            |

...

See all reports in the [Mini Pc](<Mini Pc>) directory.

Tablets
-------

| MFG                      | Model                                            | Min Temp | Avg Temp | Samples | HWID                                                                                |
|--------------------------|--------------------------------------------------|----------|----------|---------|-------------------------------------------------------------------------------------|
| AYADEVICE                | AYA NEO FOUNDER                                  | 27       | 27       | 1       | [49BC](<Tablet/AYADEVICE/AYA/AYA NEO FOUNDER/49BCDD789BF3>)                         |
| Microsoft                | Surface Book 3                                   | 37       | 37       | 1       | [DB41](<Tablet/Microsoft/Surface/Surface Book 3/DB41C0E8A15A>)                      |
| Microsoft                | Surface Pro 6                                    | 38       | 38       | 1       | [6A56](<Tablet/Microsoft/Surface/Surface Pro 6/6A5645BCB751>)                       |
| Hewlett-Packard          | Spectre x2 Detachable 12-c0XX                    | 40       | 40       | 1       | [0979](<Tablet/Hewlett-Packard/Spectre/Spectre x2 Detachable 12-c0XX/09798ACE135C>) |
| Microsoft                | Surface Pro 7                                    | 41       | 47.5     | 2       | [EEBF](<Tablet/Microsoft/Surface/Surface Pro 7/EEBF089D862A>)                       |
| Lenovo                   | MIIX 3-1030 80HV                                 | 42       | 42       | 1       | [9948](<Tablet/Lenovo/MIIX/MIIX 3-1030 80HV/99489B04E208>)                          |
| Lenovo                   | MIIX 320-10ICR 80XF                              | 43       | 43       | 1       | [36A5](<Tablet/Lenovo/MIIX/MIIX 320-10ICR 80XF/36A56AF0B943>)                       |
| Acer                     | Switch SW312-31                                  | 44       | 44       | 1       | [BBD4](<Tablet/Acer/Switch/Switch SW312-31/BBD489037843>)                           |
| Microsoft                | Surface 3                                        | 45       | 64       | 2       | [E7A3](<Tablet/Microsoft/Surface/Surface 3/E7A31022B17F>)                           |
| Microsoft                | Surface Pro 2                                    | 45       | 48.5     | 2       | [EBD1](<Tablet/Microsoft/Surface/Surface Pro 2/EBD1A15E122E>)                       |
| Hewlett-Packard          | Stream 7 Tablet                                  | 46       | 46       | 1       | [2CFE](<Tablet/Hewlett-Packard/Stream/Stream 7 Tablet/2CFECD3BB696>)                |
| Lenovo                   | MIIX 520-12IKB 81CG                              | 46       | 46       | 1       | [9B85](<Tablet/Lenovo/MIIX/MIIX 520-12IKB 81CG/9B85342702DA>)                       |
| Hampoo                   | I1D6_C109S_Hi10Pro                               | 47       | 47       | 1       | [C56D](<Tablet/Hampoo/I1/I1D6_C109S_Hi10Pro/C56DEEA57CE7>)                          |
| Microsoft                | Surface Book 2                                   | 47       | 47       | 1       | [44CB](<Tablet/Microsoft/Surface/Surface Book 2/44CB94E9B7BC>)                      |
| Kiano                    | IntelectX3HD                                     | 48       | 48       | 1       | [0765](<Tablet/Kiano/IntelectX3/IntelectX3HD/07656EC371DC>)                         |
| Lenovo                   | ThinkPad 10 20C10025SC                           | 50       | 50       | 1       | [39E1](<Tablet/Lenovo/ThinkPad/ThinkPad 10 20C10025SC/39E1A0852C8F>)                |
| Lenovo                   | ThinkPad X1 Tablet Gen 3 20KJ0011US              | 50       | 50       | 1       | [7240](<Tablet/Lenovo/ThinkPad/ThinkPad X1 Tablet Gen 3 20KJ0011US/7240D66F8895>)   |
| Medion                   | E1235T MD99743                                   | 50       | 50       | 1       | [9625](<Tablet/Medion/E1235T/E1235T MD99743/96252784B718>)                          |
| Microsoft                | Surface Pro 3                                    | 51       | 53.5     | 4       | [9F00](<Tablet/Microsoft/Surface/Surface Pro 3/9F009EA24C6E>)                       |
| Dell                     | Latitude 5175                                    | 54       | 54       | 1       | [2F00](<Tablet/Dell/Latitude/Latitude 5175/2F00E6813E75>)                           |
| Lenovo                   | MIIX 310-10ICR 80SG                              | 56       | 57.5     | 2       | [CE3B](<Tablet/Lenovo/MIIX/MIIX 310-10ICR 80SG/CE3B1405D6EA>)                       |
| ASUSTek Computer         | T102HA                                           | 58       | 58       | 1       | [C13E](<Tablet/ASUSTek Computer/T102/T102HA/C13EDC224850>)                          |
| Microsoft                | Surface Laptop Go                                | 59       | 66       | 2       | [33AF](<Tablet/Microsoft/Surface/Surface Laptop Go/33AFE4F9FBDE>)                   |
| Samsung Electronics      | Galaxy TabPro S                                  | 60       | 60       | 1       | [7232](<Tablet/Samsung Electronics/Galaxy/Galaxy TabPro S/7232B6211ED3>)            |
| AXDIA International      | PRIME WIN 12                                     | 61       | 61       | 1       | [257B](<Tablet/AXDIA International/PRIME/PRIME WIN 12/257B7AF90A21>)                |
| ZoomSmart                | A1002                                            | 62       | 62       | 1       | [F1BB](<Tablet/ZoomSmart/A/A1002/F1BBB3E02B52>)                                     |
| Dell                     | Latitude 7275                                    | 64       | 64       | 1       | [745F](<Tablet/Dell/Latitude/Latitude 7275/745FAA820535>)                           |
| Hometech                 | Wi11T                                            | 65       | 65       | 1       | [9657](<Tablet/Hometech/Wi/Wi11T/9657FC297AAB>)                                     |
| Panasonic                | FZ-G1ASH39E3                                     | 67       | 67       | 1       | [6F2F](<Tablet/Panasonic/FZ-G1/FZ-G1ASH39E3/6F2FB083615E>)                          |

...

See all reports in the [Tablet](<Tablet>) directory.

